<?php

class FetchDetails
{
    public function getAbn()
    {
        $file = fopen('_analytics/final-logs.csv', 'w');
        fputcsv($file, array("DATE", "ENDPOINT", "SINGLE HOST", "UNIQUE HOST", "CHROME", "FIREFOX", "OPERA", "SAFARI", "IE"));

        $abnLogs = file_get_contents('abn-logs.json', true);
        $logs = json_decode($abnLogs , true);

        $currentDate = 0;
        $tempEndpoint = [];
        $existingEndpoints = [];

        foreach ($logs as $details) {

            if ($currentDate !== $details['date']) {
                $tempDate = $details['date'];

                if ($currentDate !== 0) {
                    foreach ($existingEndpoints[$currentDate] as $key => $data) {
                        $analytics = [
                            'date'    => $currentDate,
                            'endpoint'  => key($existingEndpoints[$currentDate]),
                            'singleHost'     => 0,
                            'uniqueHost'      => 0,
                            'chrome'      => 1,
                            'firefox'   => 0,
                            'opera'     => 0,
                            'safari'    => 1,
                            'ie'        => 0
                        ];
                        fputcsv($file, $analytics);
                    }
                }

                $currentDate = $tempDate;

            }


            $tempEndpoint[] = $details['referrer'];

            $existingEndpoints[$details['date']][$tempEndpoint[0]][$details['agent']] += 1;

            $tempEndpoint = null;
        }
    }
}

$sample = new FetchDetails();
$sample->getAbn();